<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Quotes;
use yii\data\ActiveDataProvider;


class QuotesController extends ActiveController {
	
	public $modelClass = 'app\models\Quotes';

	public function behaviors() {
		return [
		    [
		        'class' => \yii\filters\ContentNegotiator::className(),
		        'formats' => [
		            'application/json' => \yii\web\Response::FORMAT_JSON,
		        ],
		    ],
		];
	}

	public function actionSymbol($symbol, $time) {
		
		$symbol =  strtoupper($symbol);
		switch ($time) {
			case 'm1':
				$query = Quotes::find()
				->select(['symbol','value','time'])
				->where(['symbol'=>$symbol])
				->andWhere('DATE_FORMAT(time, "%Y-%m-%d") = CURDATE()')
				->orderBy('time');
				break;

			case 'h1':
				$query = Quotes::find()
				->select(['symbol','value','time'])
				->where(['symbol'=>$symbol])
				->andWhere('DATE_FORMAT(time, "%Y-%m-%d") >= DATE_FORMAT(date_add(now(), interval -10 day), "%Y-%m-%d")')
				->orderBy('time');
				break;

			case 'd1':
				$query = Quotes::find()
				->select(['symbol','value','time'])
				->where(['symbol'=>$symbol])
				->andWhere('DATE_FORMAT(time, "%Y-%m-%d") >= DATE_FORMAT(date_add(now(), interval -60 day), "%Y-%m-%d")')
				->orderBy('time');
				break;

			default:
				$query = Quotes::find()
				->select(['symbol','value','time'])
				->where(['symbol'=>$symbol])
				->orderBy('time DESC')
				->limit(1);
				
		}
		
		$activeData = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
        return $activeData;
	}

	public function actionList() {

		return Quotes::find()->select('symbol')->distinct()->all();
	}
}