<?php

/* @var $this yii\web\View */

$this->title = 'Trading';
$serverName = Yii::$app->getRequest()->serverName;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h2>Инструкция</h2>
                <p>Для получения всех доступных символов: <a href="http://<?=$serverName?>/api/quotes/list" target="_blank">http://<?=$serverName?>/api/quotes/list</a></p>

                <p>Для получения котировок символа за указанный промежуток времени: http://<?=$serverName?>/api/quotes/<strong>SYMBOL</strong>/<strong>TIME</strong> , где:</p>
                <p><strong>SYMBOL</strong> - название символа, доступного в системе</p>
                <p><strong>TIME</strong> - получение котировок по выбранному символу, за определённый период времени, доступны значения:</p>
                <ul>
                    <li>m1 - один день;</li>
                    <li>h1 - 10  дней;</li>
                    <li>d1 - 60 дней;</li>
                    <li>last - последняя котировка;</li>
                </ul>
                <p>Например: <a href="http://<?=$serverName?>/api/quotes/EURUSD/last" target="_blank">http://<?=$serverName?>/api/quotes/EURUSD/last</a></p>
            </div>
            
        </div>

    </div>
</div>
