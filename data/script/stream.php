<?php
sleep(10);
define('REMOTE_HOST', '198.211.118.180');
define('REMOTE_PORT', 10000);
define('DB_HOST', 'mysql');
define('DB_NAME', 'trading');
define('DB_USER', 'root');
define('DB_PASS', 'qsdfgy');
define('DB_CHARSET', 'utf8');

$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHARSET;
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
];
$pdo = new PDO($dsn, DB_USER, DB_PASS, $opt);

$stream = fsockopen(REMOTE_HOST, REMOTE_PORT, $errorno, $errorstr);
stream_set_blocking($stream, 0);
stream_set_timeout($stream, 3600*24);


if (!$stream) {
   echo "$errorstr ($errorno)<br/>\n";
   echo $stream;
} else {
	
	while (true) {
		$row = fgets($stream, 128);
		if ($row) {
			list($symbol, $time, $value) = explode(";", $row);
			$time = date_create_from_format('Y-m-d?H:i:s', substr($time,2));
			
			$stmt = $pdo->prepare("INSERT INTO quotes(symbol, value, time) VALUES(:symbol, :value, :time)");
			$stmt->execute([
				':symbol' =>  substr($symbol,2),
				':value' => substr($value,2),
				':time' => $time->format('Y-m-d H:i:s')
			]);
		}
	}
	fclose($stream);	
}
